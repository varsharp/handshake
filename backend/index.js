const express = require('express');

const app = express();

const bodyParser = require('body-parser');
const cors = require('cors');

const connectDB = require('./src/db/mongoose');

require('dotenv').config();
app.use(cors({ origin: 'http://localhost:3000', credentials: true }));

app.use(express.static('uploads'));

connectDB();

const studentLoginHandler = require('./src/routes/student/entryLogin');
const companyLoginHandler = require('./src/routes/company/entryLogin');
const studentJobHandler = require('./src/routes/student/jobs');
const companyJobHandler = require('./src/routes/company/job');
const studentEventHandler = require('./src/routes/student/events');
const companyEventHandler = require('./src/routes/company/event');
const studentProfileHandler = require('./src/routes/student/profile');
const studentHandler = require('./src/routes/student/students');
const companyProfileHandler = require('./src/routes/company/profile');

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

app.use('/student', studentLoginHandler);
app.use('/jobs', studentJobHandler);
app.use('/events', studentEventHandler);
app.use('/studentprofile', studentProfileHandler);
app.use('/students', studentHandler);
app.use('/company', companyLoginHandler);
app.use('/job', companyJobHandler);
app.use('/event', companyEventHandler);
app.use('/companyprofile', companyProfileHandler);

app.listen('3001', () => {
    console.log('Handshake backend running on port 3001');
});
