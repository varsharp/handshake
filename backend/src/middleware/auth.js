const passport = require('passport');
const passportJWT = require('passport-jwt');

const ExtractJWT = passportJWT.ExtractJwt;

const JwtStrategy = passportJWT.Strategy;
const config = require('../../config');
const Student = require('../models/student');
const Company = require('../models/company');

function auth() {
  const opts = {
    jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('jwt'),
    secretOrKey: config.JWTPASSWORD,
  };

  passport.use(
    new JwtStrategy(opts, (jwt_payload, callback) => {
    const { email, user } = jwt_payload.user;
      if (user === 'student') {
        Student.findOne({email}, (err, results) => {
          if (err) {
            return callback(err, false);
          }
          if (results) {
            callback(null, results);
          } else {
            callback(null, false);
          }
        });
      } else if (user === 'company') {
        Company.findOne({email}, (err, results) => {
          if (err) {
            return callback(err, false);
          }
          if (results) {
            callback(null, results);
          } else {
            callback(null, false);
          }
        });
      }
    }),
  );
}

exports.auth = auth;
exports.checkAuth = passport.authenticate('jwt', { session: false });