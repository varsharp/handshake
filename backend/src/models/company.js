const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const CompanySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },  
  password: {
    type: String,
    required: true,
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  country: {
    type: String,
  },
  profile_picture: {
    type: String,
  },
  phone_number: {
    type: String,
  },
  user: {
    type: String,
    default: 'company'
  },
});

module.exports = Company = mongoose.model('company', CompanySchema, 'company');