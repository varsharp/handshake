import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import Login from './components/Login/Login';
import Signup from './components/Signup/Signup';
import StudentSignup from './components/Signup/StudentSignup';
import CompanySignup from './components/Signup/CompanySignup';
import CompanyLogin from './components/Login/CompanyLogin';
import Profile from './containers/Profile/profile';
import Logout from './components/Logout/logout';
import Dashboard from './containers/Dashboard/dashboard';
import Event from './containers/Event/event';
import Student from './containers/Student/student';
import Message from './containers/Message/message';
import Application from './containers/Application/application';
import CompanyProfile from './containers/CompanyProfile/company-profile';
import CreateJob from './components/create-job/create-job';
import CreateEvent from './components/create-event/create-event';
import CompanyDashboard from './containers/CompanyDashboard/company-dashboard';
import CompanyEvents from './containers/CompanyEvents/companyevents';
import { connect } from 'react-redux';

class Main extends Component {

    render() {
        let routes = (
            <Switch>                
                <Route path="/login" component={Login} />
                <Route path="/company/login" component={CompanyLogin} />
                <Route path="/signup" component={Signup} />
                <Route path="/student" component={StudentSignup} />
                <Route path="/company/signup" component={CompanySignup} />
                <Redirect to='/' />
            </Switch>           
        );
    
        if(localStorage.getItem('token')){
            routes = (
                <Switch>                               
                    <Route path="/profile" component={Profile} />
                    <Route path="/logout" component={Logout} />
                    <Route path="/dashboard" component={Dashboard} />
                    <Route path="/application" component={Application} />
                    <Route path="/event" component={Event} />
                    <Route path="/student" component={Student} />
                    <Route path="/inbox" component={Message} />
                    <Route path="/companyprofile" component={CompanyProfile} />
                    <Route path="/createjob" component={CreateJob} />
                    <Route path="/createevent" component={CreateEvent} />
                    <Route path="/company/dashboard" component={CompanyDashboard} />
                    <Route path="/company/event" component={CompanyEvents} />
                    <Redirect to='/' />
                </Switch>
            );
        }
        return (
            <div>
                {routes}
            </div>
        );
    };
};

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.login.token !== null 
    }
};

export default connect(mapStateToProps)(Main);