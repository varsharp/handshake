import React from 'react';
import { Card, Image, Button, Row, Col, Form } from 'react-bootstrap';
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { faCamera } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const ContactInfo = (props) => {
    let content;
    
    if(props.mode){
        content = (
            <div>
                <Row className="justify-content-center">
                    <Card.Title>{props.phone_number ? props.phone_number : ''}</Card.Title>
                </Row>
                <Row className="justify-content-center">
                    <Card.Text>
                        {props.email ? props.email : ''}
                    </Card.Text>
                </Row>
            </div>
        );
    } else {
        content = (
            <div>
                <Form onSubmit={props.submitHandler}>
                    <Row>
                        <Col>
                            <Form.Label>
                                Phone Number
                            </Form.Label>
                            <Form.Control defaultValue={props.contact_info ? props.contact_info.phone_number : ''} placeholder = "Please enter phone number" />
                        </Col>                  
                    </Row>
                    <Row>
                        <Col>
                        <Form.Label>
                                Email Address
                        </Form.Label>
                        <Form.Control type="email" defaultValue={props.contact_info ? props.contact_info.email : ''} placeholder = "Please enter email address" />
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col>
                            <Button type="submit" variant="success">Save</Button>
                            <Button type="button" className="ml-2" variant="danger" onClick={props.modeHandler}>Cancel</Button>
                        </Col>
                    </Row>
                </Form>
            </div>            
        )
    }
        
    return (
        <Card bg="light">
            <Card.Body>
            {!props.viewId && <Row><Button variant="link" style={{paddingLeft: '680px'}} onClick={props.modeHandler}><FontAwesomeIcon icon={faEdit} /></Button></Row>}
            <Card.Title>Contact Information</Card.Title>
            {content}
            </Card.Body>
        </Card>
    );

}