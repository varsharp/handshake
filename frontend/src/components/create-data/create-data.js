import React from 'react';
import { Button, Card, Badge, Pagination, Row, Col, Dropdown } from 'react-bootstrap';
import PDFViewer from 'pdf-viewer-reactjs';

export const CreateData = (props) => {
    let items = [];
    for (let number = 1; number <= 5; number++) {
        items.push(
            <Pagination.Item key={number} active={number === props.currentPage}>
                {number}
            </Pagination.Item>
        );
    };

    const paginationBasic = (
    <div>
        <Pagination className="float-right pt-2" onClick={ props.pageChanged }>{items}</Pagination>
    </div>
    );

    let datalist = [];
    if(props.jobs) {
        let jobs = props.jobs;
        datalist = Object.keys(jobs).map(key =>
            <Card bg="light" className = "mt-2">
                <Card.Body>
                <Button type="button" variant="link" className="p-0" onClick={ () => props.getStudents(jobs[key]._id) }>{jobs[key].title}</Button>
                <Card.Text id="type">
                    {jobs[key].job_type}
                </Card.Text>
                <Card.Text id="location">
                    {jobs[key].location}
                </Card.Text>
                <Card.Text id="salary">
                    ${jobs[key].salary} / hour
                </Card.Text>
                <Card.Text id="posting_date">
                    {jobs[key].posting_date}
                </Card.Text>          
                </Card.Body>
            </Card>
        );
    } else if(props.events) {
        let events = props.events;
        datalist = Object.keys(events).map(key =>
            <Card bg="light" className = "mt-2">
                <Card.Body>
                <Button type="button" variant="link" className="p-0" onClick={ () => props.getStudents(events[key]._id) }>{events[key].name}</Button>
                <Card.Text id="desc">
                    {events[key].description}
                </Card.Text>
                <Card.Text id="location">
                    {events[key].city}, {events[key].state}, {events[key].country}
                </Card.Text>
                <Card.Text id="eligibility">
                    {events[key].eligibility} majors
                </Card.Text>
                <Card.Text id="posting_date">
                    {events[key].date}
                </Card.Text>          
                </Card.Body>
            </Card>
        );
    }
    
    let studentlist = [];
    if(props.students) {
        let students = props.students;
        studentlist = Object.keys(students).map(key =>
            <Card bg="light" className = "mt-2">
                {props.studentId === students[key]._id && props.status && <Badge variant={props.status === 'Reviewed' ? "success" : props.status === 'Pending' ? "warning" : "danger"}>{props.status}</Badge>}
                <Card.Body>
                <Card.Text id="fname">
                <Button type="button" variant="link" className="p-0" onClick={() => props.viewProfile(students[key])}>
                    {students[key].first_name}{' '}{students[key].last_name}
                </Button>
                <Dropdown className="float-right">
                    <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                        Application Status
                    </Dropdown.Toggle>
                    <Dropdown.Menu onClick={ (event) => props.updateStatus(event, students[key]._id) }>
                        <Dropdown.Item>Pending</Dropdown.Item>
                        <Dropdown.Item>Reviewed</Dropdown.Item>
                        <Dropdown.Item>Declined</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                </Card.Text>                
                <PDFViewer
                    document={{
                        url: 'http://localhost:3001/CMPE 273 - Lab 1 Demo.pdf'
                    }}
                    hideNavbar
                />
                </Card.Body>
            </Card>
        );
    }

    return (
        <div>
            <Row>
                <Col lg={5}>{datalist}</Col>
                <Col lg={7}>{studentlist}</Col>            
            </Row>
            {paginationBasic}
        </div>      
    );
}