import React from 'react';
import { Form, Button, Col } from 'react-bootstrap';

export const StudentSearch = (props) => {
    return (
        <Form onSubmit={props.submitHandler}>
            <Form.Row>
                <Form.Group as={Col} md="3" controlId="name">
                    <Form.Label>Search by student name</Form.Label>
                    <Form.Control type="text" />
                    <Form.Control.Feedback type="invalid">
                        Please provide a valid input.
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} md="3" controlId="college_name">
                    <Form.Label>Search by college name</Form.Label>
                    <Form.Control type="text" />
                    <Form.Control.Feedback type="invalid">
                        Please provide a valid input.
                </Form.Control.Feedback>
                </Form.Group>
                <Form.Group as={Col} md="3" controlId="validationCustom04">
                    <Button type="submit" style={{ marginTop: '32px' }}>Search</Button>
                </Form.Group>
            </Form.Row>
            <Form.Row>
                <Form.Group as={Col} md="3" controlId="majorFilter">
                    <Form.Label>Enter major in format 'Computer Science'</Form.Label>
                    <Form.Control type="text" />
                </Form.Group>
            </Form.Row>
        </Form>
    );
}