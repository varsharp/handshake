import React from 'react';
import { Card, Button, Pagination } from 'react-bootstrap';

export const Students = (props) => {
    let items = [];
    for (let number = 1; number <= 5; number++) {
        items.push(
            <Pagination.Item key={number} active={number === props.currentPage}>
                {number}
            </Pagination.Item>
        );
    };

    const paginationBasic = (
    <div>
        <Pagination className="float-right pt-2" onClick={ props.pageChanged }>{items}</Pagination>
    </div>
    );
    
    let students = props.students;
    if(props.searchResults.length){
        students = props.searchResults;
    }
    const list = Object.keys(students).map(key =>
        <Card bg="light" className = "mt-2">
            <Card.Body>
            <Button type="button" variant="link" className="p-0" onClick={() => props.viewProfile(students[key])}>{students[key].first_name} {students[key].last_name}</Button>       
            </Card.Body>
        </Card>
    );
    return (
        <div>
            {list}
            {paginationBasic}
        </div>
    );  
}