import * as actionTypes from '../../../constants/action-types';

const initialState = {
    applications: [],
    searchResults: [],
    currentPage: 1,
}
 
const applicationReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SAVE_APPLICATIONS:
            return {
                ...state,
                applications: action.payload,                
            }
        case actionTypes.SET_CURRENT_APPLICATION_PAGE:
            return {
                ...state,
                currentPage: action.payload,                
            }
        case actionTypes.RETURN_APPLIED_JOBS:
            return {
                ...state,
                searchResults: action.payload               
            }
        default:
            return initialState;
    }
}

export default applicationReducer;