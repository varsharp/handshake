import * as actionTypes from '../../../constants/action-types';

const initialState = {
    jobs: null,
    students: null,
    status: null,
    currentPage: 1
}
 
const companyDashboardReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SAVE_JOBS:
            return {
                ...state,
                jobs: action.payload,                
            }
        case actionTypes.SAVE_STUDENTS:
            return {
                ...state,
                students: action.payload,                
            }
        case actionTypes.UPDATE_APPLICATION_STATUS:
            return {
                ...state,
                status: action.payload,                
            }
        // case actionTypes.SAVE_RESUME:
        //     return {
        //         ...state,
        //         resume: action.payload,                
        //     }
        case actionTypes.SET_CURRENT_COMPANY_JOB_PAGE:
            return {
                ...state,
                currentPage: action.payload,                
            }
        default:
            return initialState;
    }
}

export default companyDashboardReducer;