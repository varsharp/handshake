import * as actionTypes from '../../../constants/action-types';

export const saveEvents = (payload) => {
    return { type: actionTypes.SAVE_COMPANY_EVENTS, payload}
};

export const saveStudents = (payload) => {
    return { type: actionTypes.SAVE_COMPANY_EVENT_STUDENTS, payload}
};

export const setCurrentCompanyEventPage = (payload) => {
    return { type: actionTypes.SET_CURRENT_COMPANY_EVENT_PAGE, payload }
};
