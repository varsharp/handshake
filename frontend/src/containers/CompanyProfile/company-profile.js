import React, { Component } from 'react';
import { CompanyDetails } from '../../components/company-details/company-details.js';
import { CompanyDescription } from '../../components/company-description/company-description.js';
import { ContactInfo } from '../../components/contact-info/contact-info.js';
import { Container, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import axios from 'axios';
import { PATH } from '../../config';
import { withRouter } from 'react-router-dom';
import { saveCompanyInfo, saveContactInfo, changeMode, enableSave, saveCompanyProfilePic } from './store/action';

class CompanyProfile extends Component {
    viewId;

    componentDidMount(){
        if(this.props.location && this.props.location.search) {
            this.viewId = this.props.location.search.split('=')[1];
        }

        this.getCompanyInfo();
    }

    getCompanyInfo = () => {
        axios.defaults.headers.common['authorization'] = localStorage.getItem('token');
        let path;
        if(this.viewId) {
            let params = new URLSearchParams();
            params.set('viewId', this.viewId);
            path = PATH  + "/companyprofile/details?" + params.toString();
        } else {
            path = PATH  + "/companyprofile/details";
        }
        
        axios.get(path)
        .then(res => {
            if(res.status === 200){
                if(res.data && res.data.companyInfo.responseMessage){
                    if(res.data.companyInfo.responseMessage.profile_picture) {
                        this.props.saveCompanyProfilePic(PATH + "/" + res.data.companyInfo.responseMessage.profile_picture);
                    }
                    this.props.saveCompanyInfo({...res.data.companyInfo.responseMessage, editMode:false});
                    
                    if(res.data.companyInfo.responseMessage.description){
                        this.props.save = false;
                    }
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    changeMode = (event) => {
        if(event.target.innerText === 'Cancel' || event.target.innerText === 'Save'){
            this.props.changeMode(false);
        } else {
            this.props.changeMode(true);
        }
    }

    enableSave = (event) => {
        if(!event){
            this.props.enableSave(false);
        } else {
            this.props.enableSave(true);
        }        
    }

    addProfilePic = (event) => {
        event.preventDefault();
        const formData = new FormData();
        const file = event.target.elements[0].files[0];
        formData.append('profile_pic', event.target.elements[0].files[0]);
        formData.append('id', this.props.companyInfo._id);
        axios.post(PATH + "/companyprofile/profilepic", formData, {
            headers: {
                'content-type':'multipart/form-data'
            }
        })
        .then(res => {
            if(res.status === 200){
                this.props.saveCompanyProfilePic(PATH + "/" + file.name);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    enableContactSave = (event) => {
        if(!event){
            this.props.enableContactSave(false);
        } else {
            this.props.enableContactSave(true);
        }        
    }

    changeContactMode = (event) => {
        if(event.target.innerText === 'Cancel' || event.target.innerText === 'Save'){
            this.props.changeContactMode(false);
        } else {
            this.props.changeContactMode(true);
        }
    }

    enableSave = (event) => {
        if(!event){
            this.props.enableSave(false);
        } else {
            this.props.enableSave(true);
        }        
    }

    changeMode = (event) => {
        if(event.target.innerText === 'Cancel' || event.target.innerText === 'Save'){
            this.props.changeMode(false);
        } else {
            this.props.changeMode(true);
        }
    }

    saveDescription = (event) => {
        event.preventDefault();
        axios.post(PATH + "/companyprofile/details", this.props.companyInfo)
        .then(res => {
            if(res.status === 200){
                this.props.enableSave(false);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    updateDescription = (event) => {
        let newDetails = {};
        Object.assign(newDetails, this.props.companyInfo);
        newDetails.description = event.target.value;
        this.props.saveCompanyInfo(newDetails);
    }

    saveContactInfo = (event) => {
        event.preventDefault();
        axios.post(PATH + "/companyprofile/details", this.props.companyInfo)
        .then(res => {
            if(res.status === 200){
                this.props.enableContactMode(false);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    updateContactInfo = (event) => {
        let newDetails = {};
        Object.assign(newDetails, this.props.companyInfo);
        newDetails.phone_number = event.target.elements[0].value;
        newDetails.email = event.target.elements[0].value;
        this.props.saveCompanyInfo(newDetails);
    }

    render(){
            return (
                <Container className="mt-5 mb-5">
                    <Row>
                        <Col sm={4} md={4} lg={4}>
                            <CompanyDetails details = { this.props.companyInfo } viewId = { this.viewId } addProfilePic = { this.addProfilePic } profilePic = { this.props.profile_pic } modeHandler = { this.changeMode } mode = { this.props.mode }></CompanyDetails><br/>
                        </Col>
                        <Col sm={8} md={8} lg={8}>
                            <CompanyDescription description = { this.props.companyInfo ? this.props.companyInfo.description : '' } viewId = { this.viewId } enableSave = { this.enableSave } updateHandler = {this.updateDescription} submitHandler={this.saveDescription} save = {this.props.save}></CompanyDescription><br/>
                            <ContactInfo phone_number = { this.props.companyInfo ? this.props.companyInfo.phone_number : '' } email = { this.props.companyInfo ? this.props.companyInfo.email : '' } viewId = { this.viewId } mode = {true}></ContactInfo><br/>
                        </Col>
                    </Row>
                </Container>            
            )     
    }
}

const mapStateToProps = (state) => {
    return {
        companyInfo: state.companyprofile.companyInfo,
        contactInfo: state.companyprofile.contactInfo,
        mode: state.companyprofile.mode,
        save: state.companyprofile.save,
        profile_pic: state.companyprofile.profile_pic,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveCompanyInfo: (data) => dispatch(saveCompanyInfo(data)),
        saveContactInfo: (data) => dispatch(saveContactInfo(data)),
        changeMode: (data) => dispatch(changeMode(data)),
        enableSave: (data) => dispatch(enableSave(data)),
        saveCompanyProfilePic: (data) => dispatch(saveCompanyProfilePic(data)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CompanyProfile));