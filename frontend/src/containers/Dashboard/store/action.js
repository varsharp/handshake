import * as actionTypes from '../../../constants/action-types';

export const saveJobs = (payload) => {
    return { type: actionTypes.SAVE_JOBS, payload}
};

export const returnJobs = (payload) => {
    return { type: actionTypes.RETURN_JOBS, payload}
};

export const controlModal = (payload) => {
    return { type: actionTypes.CONTROL_MODAL, payload}
};

export const saveResume = (payload) => {
    return { type: actionTypes.SAVE_RESUME, payload }
};

export const applyToJob = (payload) => {
    return { type: actionTypes.APPLY_TO_JOB, payload }
};

export const setCurrentJobPage = (payload) => {
    return { type: actionTypes.SET_CURRENT_JOB_PAGE, payload }
};

export const setSearchStr = (payload) => {
    return { type: actionTypes.SET_STUDENT_JOB_SEARCH_STR, payload }
};

export const setSearchVal = (payload) => {
    return { type: actionTypes.SET_STUDENT_JOB_SEARCH_VAL, payload }
};

export const setSortByValue = (payload) => {
    return { type: actionTypes.SET_SORT_BY_VALUE, payload }
};

export const setSortByOrder = (payload) => {
    return { type: actionTypes.SET_SORT_BY_ORDER, payload }
};