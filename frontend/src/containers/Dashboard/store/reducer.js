import * as actionTypes from '../../../constants/action-types';

const initialState = {
    jobs: [],
    searchResults: [],
    currentPage: 1,
    searchStr: '',
    searchVal: '',
    sortByValue: '',
    sortByOrder: 1,
    openModal: false,
    resume: "",
    success: false
}
 
const jobReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SAVE_JOBS:
            return {
                ...state,
                jobs: action.payload,                
            }
        case actionTypes.RETURN_JOBS:
            return {
                ...state,
                searchResults: action.payload,                
            }
        case actionTypes.CONTROL_MODAL:
            return {
                ...state,
                openModal: action.payload,                
            }
        case actionTypes.SAVE_RESUME:
            return {
                ...state,
                resume: action.payload,                
            }
        case actionTypes.APPLY_TO_JOB:
            return {
                ...state,
                success: action.payload,                
            }
        case actionTypes.SET_CURRENT_JOB_PAGE:
            return {
                ...state,
                currentPage: action.payload,                
            }
        case actionTypes.SET_STUDENT_JOB_SEARCH_STR:
            return {
                ...state,
                searchStr: action.payload,                
            }
        case actionTypes.SET_STUDENT_JOB_SEARCH_VAL:
            return {
                ...state,
                searchVal: action.payload,                
            }
        case actionTypes.SET_SORT_BY_VALUE:
            return {
                ...state,
                sortByValue: action.payload,                
            }
        case actionTypes.SET_SORT_BY_ORDER:
            return {
                ...state,
                sortByOrder: action.payload,              
            }
        default:
            return initialState;
    }
}

export default jobReducer;