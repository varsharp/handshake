import * as actionTypes from '../../../constants/action-types';

export const saveEvents = (payload) => {
    return { type: actionTypes.SAVE_EVENTS, payload}
};

export const returnEvents = (payload) => {
    return { type: actionTypes.RETURN_EVENTS, payload}
};

export const controlModal = (payload) => {
    return { type: actionTypes.CONTROL_EVENT_MODAL, payload}
};

export const registerFailure = (payload) => {
    return { type: actionTypes.REGISTER_FAILURE, payload }
};

export const registerSuccess = (payload) => {
    return { type: actionTypes.REGISTER_SUCCESS, payload }
};

export const setCurrentEventPage = (payload) => {
    return { type: actionTypes.SET_CURRENT_EVENT_PAGE, payload }
};

export const setSearchVal = (payload) => {
    return { type: actionTypes.SET_STUDENT_EVENT_SEARCH_VAL, payload }
};
