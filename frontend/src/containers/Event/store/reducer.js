import * as actionTypes from '../../../constants/action-types';

const initialState = {
    events: [],
    currentPage: 1,
    searchVal: undefined,
    searchResults: [],
    openModal: false,
    success: false,
    failure: null
}
 
const eventReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SAVE_EVENTS:
            return {
                ...state,
                events: action.payload,                
            }
        case actionTypes.RETURN_EVENTS:
            return {
                ...state,
                searchResults: action.payload,                
            }
        case actionTypes.SET_CURRENT_EVENT_PAGE:
            return {
                ...state,
                currentPage: action.payload,               
            }
        case actionTypes.SET_STUDENT_EVENT_SEARCH_VAL:
            return {
                ...state,
                searchVal: action.payload,                
            }
        case actionTypes.CONTROL_EVENT_MODAL:
            return {
                ...state,
                openModal: action.payload,                
            }
        case actionTypes.REGISTER_SUCCESS:
            return {
                ...state,
                success: action.payload,
            }
        case actionTypes.REGISTER_FAILURE:
            return {
                ...state,
                failure: action.payload, 
            }
        default:
            return initialState;
    }
}

export default eventReducer;