import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import axios from 'axios';
import { PATH } from '../../config';
import { connect } from 'react-redux';
import { saveMessages, saveHistory, saveReply } from './store/action';
import { MessageComponent } from '../../components/message/message';

class Message extends Component {
    msg = null;

    componentDidMount() {
        this.getMessages();
    }

    getMessages = () => {        
        axios.defaults.headers.common['authorization'] = localStorage.getItem('token');
        let path;
        if(localStorage.getItem('user') === 'student') {
            path = PATH + "/studentprofile/messages";
        } else {
            path = PATH + "/companyprofile/messages";
        }
        axios.get(path)
        .then(res => {
            if(res.status === 200){
                if(res.data && res.data.messages.responseMessage){
                    this.props.saveMessages(res.data.messages.responseMessage.message_list);
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    getHistory = (msg) => {
        this.msg = msg;
        let message = this.props.messages.find(message => message._id === msg._id);
        if(message) {
            this.props.saveHistory(message.message_conversation);
        }
    }

    sendMessage = (e) => {
        e.preventDefault();
        let path;
        if(localStorage.getItem('user') === 'student') {
            path = PATH + "/studentprofile/message";
        } else {
            path = PATH + "/companyprofile/message";
        }
        let data = {email: this.msg.email, body: this.props.reply};
        axios.post(path, data)
        .then(res => {
            if(res.status === 200 && res.data.message.status === 200){
                this.props.saveHistory(res.data.message.responseMessage);
                document.getElementById('message-form').reset();
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })  
    }

    updateMsgHandler = (e) => {
        this.props.saveReply(e.target.value);
    }

    render() {
        return (            
            <Container className="mt-5 mb-5">
                <h1 className="display-4">Inbox</h1>
                { this.props.messages && this.props.messages.length && 
                <div className="w-100 bg-light text-dark mt-5 p-5 shadow rounded">
                    <MessageComponent messages = { this.props.messages } getHistory = {this.getHistory} history = {this.props.history} msg = {this.msg} updateMsgHandler = {this.updateMsgHandler} sendMessage = {this.sendMessage}></MessageComponent>
                </div> }
            </Container>
        )
    };
};

const mapStateToProps = (state) => {
    return {
        messages: state.msg.messages,
        history: state.msg.history,
        reply: state.msg.reply
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveMessages: (data) => dispatch(saveMessages(data)),
        saveHistory: (data) => dispatch(saveHistory(data)),
        saveReply: (data) => dispatch(saveReply(data)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Message);