import * as actionTypes from '../../../constants/action-types';

export const saveMessages = (payload) => {
    return { type: actionTypes.SAVE_MESSAGES, payload }
};

export const saveHistory = (payload) => {
    return { type: actionTypes.SAVE_HISTORY, payload }
};

export const saveReply = (payload) => {
    return { type: actionTypes.SAVE_REPLY, payload }
};
