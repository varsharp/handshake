import React, { Component } from 'react';
import { BasicDetails } from '../../components/basic-details/basic-details';
import { CareerObjective } from '../../components/career-objective/career-objective';
import { Education } from '../../components/education/education';
import { Experience } from '../../components/experience/experience';
import { Skillset } from '../../components/skillset/skillset';
import { Container, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import axios from 'axios';
import { PATH } from '../../config';
import { withRouter } from 'react-router-dom';
import { saveBasicDetails, saveEducationInfo, saveExperienceInfo, saveSkillset, changeMode, changeEdKey, changeExpKey, enableSave, saveProfilePic, changeEdMode, changeExpMode, controlMessageModal, sendMessage, saveMessage } from './store/action';

class Profile extends Component {
    viewId;
    constructor(){
        super();
        this.updateCareerObj = this.updateCareerObj.bind(this);
        this.deleteEducation = this.deleteEducation.bind(this);
        this.deleteExperience = this.deleteExperience.bind(this);
    }

    componentDidMount(){
        if(this.props.location && this.props.location.search) {
            this.viewId = this.props.location.search.split('=')[1];
        }

        this.getBasicDetails();
        this.getEducationInfo();
        this.getExperienceInfo();
        this.getSkillset();
    }

    getBasicDetails = () => {
        axios.defaults.headers.common['authorization'] = localStorage.getItem('token');
        let path;
        if(this.viewId) {
            let params = new URLSearchParams();
            params.set('viewId', this.viewId);
            path = PATH  + "/studentprofile/details?" + params.toString();
        } else {
            path = PATH  + "/studentprofile/details";
        }
        
        axios.get(path)
        .then(res => {
            if(res.status === 200){
                if(res.data && res.data.basicDetails.responseMessage){
                    if(res.data.basicDetails.responseMessage.profile_picture) {
                        this.props.saveProfilePic(PATH + "/" + res.data.basicDetails.responseMessage.profile_picture);
                    }
                    this.props.saveBasicDetails({...res.data.basicDetails.responseMessage, editMode:false});
                    
                    if(res.data.basicDetails.responseMessage.career_objective){
                        this.props.save = false;
                    }
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    getEducationInfo = () => {
        let path;
        if(this.viewId) {
            let params = new URLSearchParams();
            params.set('viewId', this.viewId);
            path = PATH + "/studentprofile/education?" + params.toString();
        } else {
            path = PATH + "/studentprofile/education";
        }
        
        axios.get(path)
        .then(res => {
            if(res.status == 200){
                if(res.data && res.data.education.responseMessage){
                    this.props.saveEducationInfo(res.data.education.responseMessage.education);
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    updateEducationInfo = (value) => {
        let newEducation = [];
        Object.assign(newEducation, this.props.education);
        if(this.props.edKey) {
            newEducation[this.props.edKey] = value;
        } else {
            newEducation.push(value);
        }       
          
        this.props.saveEducationInfo(newEducation);
    }

    saveEducationInfo = (event) => {
        event.preventDefault();
        const data = {
            "college_name": event.target.elements[0].value,            
            "degree": event.target.elements[1].value,
            "year_of_passing": event.target.elements[2].value,
            "major": event.target.elements[3].value,
            "city": event.target.elements[4].value,
            "state": event.target.elements[5].value,            
            "country": event.target.elements[6].value,
            "cgpa": event.target.elements[7].value           
        }
        axios.post(PATH + "/studentprofile/education", data)
        .then(res => {
            if(res.status === 200){
                localStorage.setItem('major', data.major);
                this.updateEducationInfo(data);
                this.changeEdMode(false);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    editEducationInfo = (event) => {
        event.preventDefault();
        const data = {
            "college_name": event.target.elements[0].value,            
            "degree": event.target.elements[1].value,
            "year_of_passing": event.target.elements[2].value,
            "major": event.target.elements[3].value,
            "city": event.target.elements[4].value,
            "state": event.target.elements[5].value,            
            "country": event.target.elements[6].value,
            "cgpa": event.target.elements[7].value           
        }
        data._id = this.props.education[this.props.edKey]._id;
        axios.post(PATH + "/studentprofile/updateeducation", data)
        .then(res => {
            if(res.status === 200){
                localStorage.setItem('major', data.major);
                this.updateEducationInfo(data);
                this.changeEdMode(false);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    deleteEducation(key, id) {
        let newEducation = [];
        Object.assign(newEducation, this.props.education);
        newEducation.splice(Number(key), 1);
        axios.delete(PATH + "/studentprofile/education?_id=" + id)
        .then(res => {
            if(res.status === 200){
                this.props.saveEducationInfo(newEducation);
                this.changeEdMode(false);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
        
    }

    changeEdMode = (mode, key) => {
        this.props.changeEdMode(mode);
        this.props.changeEdKey(key);
    }

    changeExpMode = (mode, key) => {
        this.props.changeExpMode(mode);
        this.props.changeExpKey(key);
    }

    getExperienceInfo = () => {
        let path;
        if(this.viewId) {
            let params = new URLSearchParams();
            params.set('viewId', this.viewId);
            path = PATH + "/studentprofile/experience?" + params.toString();
        } else {
            path = PATH + "/studentprofile/experience";
        }
        
        axios.get(path)
        .then(res => {
            if(res.status === 200){
                if(res.data && res.data.experience.responseMessage){
                    this.props.saveExperienceInfo(res.data.experience.responseMessage.experience);
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    updateExperienceInfo = (value) => {
        let newExperience = [];
        Object.assign(newExperience, this.props.experience);
        if(this.props.expKey) {
            newExperience[this.props.expKey] = value;
        } else {
            newExperience.push(value);
        }           
        this.props.saveExperienceInfo(newExperience);
    }

    saveExperienceInfo = (event) => {
        event.preventDefault();
        const data = {
            "company": event.target.elements[0].value,            
            "title": event.target.elements[1].value,
            "start_date": event.target.elements[2].value,
            "end_date": event.target.elements[3].value,
            "city": event.target.elements[4].value,
            "state": event.target.elements[5].value,
            "country": event.target.elements[6].value,
            "description": event.target.elements[7].value     
        }
        axios.post(PATH + "/studentprofile/experience", data)
        .then(res => {
            if(res.status === 200 && res.data.experience.responseMessage === 'Successful'){
                this.updateExperienceInfo(data);
                this.changeExpMode(false);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    editExperienceInfo = (event) => {
        event.preventDefault();
        const data = {
            "company": event.target.elements[0].value,            
            "title": event.target.elements[1].value,
            "start_date": event.target.elements[2].value,
            "end_date": event.target.elements[3].value,
            "city": event.target.elements[4].value,
            "state": event.target.elements[5].value,
            "country": event.target.elements[6].value,
            "description": event.target.elements[7].value         
        }
        data._id = this.props.experience[this.props.expKey]._id;
        axios.post(PATH + "/studentprofile/updateexperience", data)
        .then(res => {
            if(res.status === 200){
                localStorage.setItem('major', data.major);
                this.updateExperienceInfo(data);
                this.changeExpMode(false);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    deleteExperience(key, id) {
        let newExperience = [];
        Object.assign(newExperience, this.props.experience);
        newExperience.splice(Number(key), 1);
        axios.delete(PATH + "/studentprofile/experience?_id=" + id)
        .then(res => {
            if(res.status === 200){
                this.props.saveExperienceInfo(newExperience);
                this.changeExpMode(false);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
        
    }

    getSkillset = () => {
        let path;
        if(this.viewId) {
            let params = new URLSearchParams();
            params.set('viewId', this.viewId);
            path = PATH + "/studentprofile/skillset?" + params.toString();
        } else {
            path = PATH + "/studentprofile/skillset";
        }
        
        axios.get(path)
        .then(res => {
            if(res.status == 200){
                if(res.data && res.data.skillset.responseMessage.skills){
                    this.props.saveSkillset(res.data.skillset.responseMessage.skills);
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    updateBasicDetails = (event) => {
        let newDetails = {};
        Object.assign(newDetails, this.props.basicDetails);
        newDetails.last_name = event.target.value;
        this.props.saveBasicDetails(newDetails);
    }

    saveBasicDetails = (event) => {
        event.preventDefault();
        axios.post(PATH + "/studentprofile/details", this.props.basicDetails)
        .then(res => {
            if(res.status === 200){
                this.changeMode({target : {innerText : 'Save'}});
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    changeMode = (event) => {
        if(event.target.innerText === 'Cancel' || event.target.innerText === 'Save'){
            this.props.changeMode(false);
        } else {
            this.props.changeMode(true);
        }
    }

    enableSave = (event) => {
        if(!event){
            this.props.enableSave(false);
        } else {
            this.props.enableSave(true);
        }        
    }

    updateCareerObj = (event) => {
        let newDetails = {};
        Object.assign(newDetails, this.props.basicDetails);
        newDetails.career_objective = event.target.value;
        this.props.saveBasicDetails(newDetails);
    }

    saveCareerObj = (event) => {
        event.preventDefault();
        axios.post(PATH + "/studentprofile/details", this.props.basicDetails)
        .then(res => {
            if(res.status === 200){
                this.props.enableSave(false);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    updateSkillset = (value, action) => {
        let newSkills = [];
        Object.assign(newSkills, this.props.skillset);
        let idx = newSkills.findIndex(skill => skill.toLowerCase() === value.toLowerCase());
        if(action === "remove") {
            if(idx > -1) {
                newSkills.splice(idx, 1);
            }
        }            
        else {
            if(idx === -1) {
                newSkills.push(value);
            }            
        }
        this.props.saveSkillset(newSkills);
    }

    saveSkillset = (event) => {
        event.preventDefault();
        const data =  { skill: event.target.elements[0].value };
        axios.post(PATH + "/studentprofile/skillset", data)
        .then(res => {
            if(res.status === 200){ 
                this.updateSkillset(data.skill);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    removeSkillset = (data) => {
        axios.post(PATH + "/studentprofile/skillset", data)
        .then(res => {
            if(res.status === 200){ 
                this.updateSkillset(data.skill);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    removeSkill = (skill) => {
        this.updateSkillset(skill, "remove");
    }

    addProfilePic = (event) => {
        event.preventDefault();
        const formData = new FormData();
        const file = event.target.elements[0].files[0];
        formData.append('profile_pic', event.target.elements[0].files[0]);
        formData.append('id', this.props.basicDetails.id);
        axios.post(PATH + "/studentprofile/profilepic", formData, {
            headers: {
                'content-type':'multipart/form-data'
            }
        })
        .then(res => {
            if(res.status === 200){
                this.props.saveProfilePic(PATH + "/" + file.name);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })
    }

    controlMessageModal = (action) => {
        this.props.controlMessageModal(action);
    }

    sendMessage = () => {
        let path;
        if(localStorage.getItem('user') === 'student') {
            path = PATH + "/studentprofile/message";
        } else {
            path = PATH + "/companyprofile/message"
        }
        let data = {email: this.props.basicDetails.email, body: this.props.message};
        axios.post(path, data)
        .then(res => {
            if(res.status === 200 && res.data.message.status === 200){
                this.props.sendMessage(true);
            }
        })
        .catch(err=>{
            //this.props.authFail(err.response.data.msg);
        })        
    }

    updateMsgHandler = (e) => {
        this.props.saveMessage(e.target.value);
    }

    render(){
            return (
                <Container className="mt-5 mb-5">
                    <Row>
                        <Col sm={4} md={4} lg={4}>
                            <BasicDetails details={this.props.basicDetails} education={this.props.education} updateHandler = {this.updateBasicDetails} submitHandler={this.saveBasicDetails} mode = {this.props.mode} modeHandler = {this.changeMode} addProfilePic = {this.addProfilePic} profilePic = { this.props.profile_pic } viewId = {this.viewId} controlMessageModal = {this.controlMessageModal} sendMessage = {this.sendMessage} success = { this.props.success} updateMsgHandler = {this.updateMsgHandler} showMessageModal = {this.props.showMessageModal}></BasicDetails><br/>
                            <Skillset skills={this.props.skillset} submitHandler={this.saveSkillset} removeHandler = { this.removeSkill } viewId = {this.viewId} ></Skillset>
                        </Col>
                        <Col sm={8} md={8} lg={8}>
                            <CareerObjective career_obj = {this.props.basicDetails ? this.props.basicDetails.career_objective : ''} updateHandler = {this.updateCareerObj} submitHandler={this.saveCareerObj} save = {this.props.save} enableSave={this.enableSave} viewId = {this.viewId} ></CareerObjective><br/>
                            <Education education = {this.props.education} submitHandler = {this.saveEducationInfo} changeEdMode = {this.changeEdMode} edMode = {this.props.edMode} edKey = {this.props.edKey} editEducationInfo = {this.editEducationInfo} deleteEducation = {this.deleteEducation} viewId = {this.viewId} ></Education><br/>
                            <Experience experience = {this.props.experience} submitHandler = {this.saveExperienceInfo} changeExpMode = {this.changeExpMode} expMode = {this.props.expMode} expKey = {this.props.expKey} editExperienceInfo = {this.editExperienceInfo} deleteExperience = {this.deleteExperience} viewId = {this.viewId} ></Experience>
                        </Col>
                    </Row>
                </Container>            
            )     
    }
}

const mapStateToProps = (state) => {
    return {
        basicDetails: state.profile.basicDetails,
        education: state.profile.education,
        experience: state.profile.experience,
        skillset: state.profile.skillset,
        mode: state.profile.mode,
        save: state.profile.save,
        profile_pic: state.profile.profile_pic,
        edMode: state.profile.edMode,
        edKey: state.profile.edKey,
        expKey: state.profile.expKey,
        expMode: state.profile.expMode,
        showMessageModal: state.profile.showMessageModal,
        success: state.profile.success,
        message: state.profile.message
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveBasicDetails: (data) => dispatch(saveBasicDetails(data)),
        saveEducationInfo: (data) => dispatch(saveEducationInfo(data)),
        saveExperienceInfo: (data) => dispatch(saveExperienceInfo(data)),
        saveSkillset: (data) => dispatch(saveSkillset(data)),
        changeMode: (data) => dispatch(changeMode(data)),
        enableSave: (data) => dispatch(enableSave(data)),
        saveProfilePic: (data) => dispatch(saveProfilePic(data)),
        changeEdMode: (data) => dispatch(changeEdMode(data)),
        changeEdKey: (data) => dispatch(changeEdKey(data)),
        changeExpKey: (data) => dispatch(changeExpKey(data)),
        changeExpMode: (data) => dispatch(changeExpMode(data)),
        controlMessageModal: (data) => dispatch(controlMessageModal(data)),
        sendMessage: (data) => dispatch(sendMessage(data)),
        saveMessage: (data) => dispatch(saveMessage(data)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Profile));