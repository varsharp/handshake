import * as actionTypes from '../../../constants/action-types';

const initialState = {
    students: [],
    searchResults: [],
    currentPage: 1,
    searchStr: '',
    searchVal: '',
    success: false
}
 
const studentReducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.SAVE_STUDENT_LIST:
            return {
                ...state,
                students: action.payload,                
            }
        case actionTypes.RETURN_STUDENTS:
            return {
                ...state,
                searchResults: action.payload,                
            }
        case actionTypes.SET_CURRENT_STUDENT_PAGE:
            return {
                ...state,
                currentPage: action.payload,                
            }
        case actionTypes.SET_STUDENT_STUDENT_SEARCH_STR:
            return {
                ...state,
                searchStr: action.payload,                
            }
        case actionTypes.SET_STUDENT_STUDENT_SEARCH_VAL:
            return {
                ...state,
                searchVal: action.payload,                
            }
        default:
            return initialState;
    }
}

export default studentReducer;