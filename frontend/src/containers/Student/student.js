import React, { Component } from 'react';
import { StudentSearch } from '../../components/student-search/student-search';
import { Students } from '../../components/students/students';
import { Container } from 'react-bootstrap';
import axios from 'axios';
import { PATH } from '../../config';
import { connect } from 'react-redux';
import { saveStudents, returnStudents, setCurrentStudentPage, setSearchStr, setSearchVal } from './store/action';

class Student extends Component {
    filters = [];

    componentDidMount() {
        this.getStudents(this.props.currentPage, '', null);
    }

    getStudents = (page, searchStr, searchVal) => {
        let params = new URLSearchParams();
        params.set(searchStr, searchVal);
        params.set('page', page);

        if(this.filters.length) {
            params.set('filter', this.filters[0])
        }
        axios.defaults.headers.common['authorization'] = localStorage.getItem('token');
        axios.get(PATH  + "/students?" + params.toString())
        .then(res => {
            if(res.status === 200){
                if(res.data){
                    this.props.saveStudents(res.data.students.responseMessage);
                }
            }
        })
        .catch(err=>{
            //this.props.setError(err.response.data);
        })
    }

    search = (event) => {
        event.preventDefault();
        this.props.setSearchStr(event.target.elements[0].getAttribute('id'));
        this.props.setSearchVal(event.target.elements[0].value.toLowerCase());
        this.getStudents(1, event.target.elements[0].getAttribute('id'), event.target.elements[0].value.toLowerCase());
    }

    recordFilters = (event) => {
        this.filters.push(event.target.innerText);
    }

    pageChanged = (e) => {
        this.props.setCurrentJobPage(Number(e.target.text));
        this.getStudents(e.target.text, this.props.searchStr, this.props.searchVal);
    };

    viewProfile = (student) => {
        let id = student._id;
        this.props.history.push({
            pathname: '/profile',
            search: '?view=' + id
        });
    }

    render() {
        return (
            <Container className="mt-5 mb-5">
                <h1 className="display-4">Student Search</h1>
                <div className="w-100 bg-light text-dark p-5 shadow rounded">
                    <StudentSearch submitHandler={ this.search } recordFilters = { this.recordFilters } ></StudentSearch>
                </div>
                <div className="w-100 bg-light text-dark mt-5 p-5 shadow rounded">
                    <Students students = { this.props.students } searchResults = { this.props.searchResults } pageChanged = { this.pageChanged } currentPage = { this.props.currentPage } viewProfile = {this.viewProfile} ></Students>
                </div>
            </Container>
        )
    };
};

const mapStateToProps = (state) => {
    return {
        students: state.student.students,
        currentPage: state.student.currentPage,
        searchStr: state.student.searchStr,
        searchVal: state.student.searchVal,
        searchResults: state.job.searchResults,
        success: state.job.success
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveStudents: (data) => dispatch(saveStudents(data)),
        returnStudents: (data) => dispatch(returnStudents(data)),
        setCurrentStudentPage: (data) => dispatch(setCurrentStudentPage(data)),
        setSearchStr: (data) => dispatch(setSearchStr(data)),
        setSearchVal: (data) => dispatch(setSearchVal(data)),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Student);