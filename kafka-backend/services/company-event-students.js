var Event = require('../models/event');

handle_request =  async(msg, callback) => {
    try {
        const registrations = await Event.find({ _id: msg.eventId }).select('registration -_id');
        const students = [];
        if (registrations && registrations[0]) {
            for( const reg of registrations[0].registration ) {
                let student = await Student.find({ _id: reg.student });
                students.push(student[0]);
            }
            
            if(students && students.length > 0) {
                callback(null, {status: 200, responseMessage: students});
            }
        }
    
        callback(null, {status: 200, responseMessage: students});
    } catch (e) {
        callback(null, {status: 500, responseMessage: 'Unable to fetch data.'});
    }
}

exports.handle_request = handle_request;
