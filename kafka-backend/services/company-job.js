var Job = require('../models/job');

handle_request =  async(msg, callback) => {
    try {
        const job = msg;
        const jobEntry = new Job({
            ...job,
            company: msg.user._id
        });
        console.log("job entry")
        console.log(jobEntry)
        await jobEntry.save();
        callback(null, {status: 200, responseMessage: "Successful"});
    } catch (e) {
        callback(null, {status: 500, responseMessage: 'Unable to save data.'});
    }
}

exports.handle_request = handle_request;
