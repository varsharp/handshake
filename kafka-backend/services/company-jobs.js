var Job = require('../models/job');

handle_request =  async(msg, callback) => {
    let options = {
        page: Number(msg.query.page),
        limit: 5,
    }
    try {
        const jobs = await Job.paginate({company: msg.user._id}, options, (err, result) => {
            return result.docs;
        })
        if (jobs) {
            callback(null, {status: 200, responseMessage: jobs});
        }
    } catch (e) {
        callback(null, {status: 500, responseMessage: 'Unable to fetch data.'});
    }
}

exports.handle_request = handle_request;
