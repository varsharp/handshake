var Student = require('../models/student');
var Company = require('../models/company');

handle_request = async (msg, callback) => {
    let { email } = msg;
    const receiverStudent = await Student.findOne({email});

    const senderCompany = await Company.findById(msg.user._id);

    const senderMessageConvEntry = {
        date: Date.now(),
        body: msg.body,
        action: 'Sent'
    };
    const receiverMessageConvEntry = {
        date: Date.now(),
        body: msg.body,
        action: 'Received'
    };

    try {
        let receiver = receiverStudent.first_name + ' ' + receiverStudent.last_name;
        let sender = senderCompany.name;
        let senderMessageEntry = {
            entity: receiver,
            date: Date.now(),
            email,
        };
        let receiverMessageEntry = {
            entity: sender,
            date: Date.now(),
            email: senderCompany.email,
        };
        
        // entry in sender
        if (senderCompany.message_list.length) {
            let history = senderCompany.message_list.find(message => message.email == email);
            if(history) {
                history.message_conversation.unshift(senderMessageConvEntry);
                await senderCompany.save();
            } else {
                senderCompany.message_list.push(senderMessageEntry);
                senderCompany.message_list[senderCompany.message_list.length - 1].message_conversation.push(senderMessageConvEntry);
                await senderCompany.save();
            }
        } else {
            senderCompany.message_list.push(senderMessageEntry);
            senderCompany.message_list[0].message_conversation.push(senderMessageConvEntry);
            await senderCompany.save();
        }

        // entry in receiver
        if (receiverStudent.message_list.length) {
            let history = receiverStudent.message_list.find(message => message.email == senderCompany.email);
            if(history) {
                history.message_conversation.unshift(receiverMessageConvEntry);
                await receiverStudent.save();
            } else {
                receiverStudent.message_list.push(receiverMessageEntry);
                receiverStudent.message_list[receiverStudent.message_list.length - 1].message_conversation.push(receiverMessageConvEntry);
                await receiverStudent.save();
            }
        } else {
            receiverStudent.message_list.push(receiverMessageEntry);
            receiverStudent.message_list[0].message_conversation.push(receiverMessageConvEntry);
            await receiverStudent.save();
        }

        callback(null, { status: 200, responseMessage: senderCompany.message_list[0].message_conversation });
    } catch (e) {
        console.log(e)
        callback(null, { status: 500, responseMessage: 'Unable to save data.' });
    }
};

exports.handle_request = handle_request;