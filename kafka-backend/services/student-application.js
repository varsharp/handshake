var Job = require('../models/job');

handle_request =  async(msg, callback) => {
    const job = await Job.findById(msg.id);
    const student = msg.user._id;
    const resume = msg.resume;
    try {
        const studentEntry = {
            status: "Pending",
            applied_date: Date.now(),
            student,
            resume
        }
		job.application.unshift(studentEntry);
        await job.save();

        callback(null, {status: 200, responseMessage: "Successful"});
    } catch (e) {
		callback(null, {status: 500, responseMessage: 'Unable to save data.'});
    }
}

exports.handle_request = handle_request;
