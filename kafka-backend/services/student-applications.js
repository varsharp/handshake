var Job = require('../models/job');
var ObjectId = require('mongoose').Types.ObjectId;

handle_request =  async(msg, callback) => {
    let id = msg.user._id;
    let query = {'application.student': new ObjectId(id)};
    if(msg.query.filter) {
        query['application.status'] = msg.query.filter;
    }    
    let options = {
        page: Number(msg.query.page),
        limit: 5,
    }
    try {
        const results = await Job.paginate(query, options, (err, result) => {
            return result.docs;
        })
        if (results) {
            results.forEach(doc => doc.application = doc.application.filter(app => app.student == id));
            callback(null, {status: 200, responseMessage: results});
        }
    } catch (e) {
		callback(null, {status: 500, responseMessage: 'Unable to fetch data.'});
    }
}

exports.handle_request = handle_request;