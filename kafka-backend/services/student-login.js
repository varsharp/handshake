var bcrypt = require('bcrypt');
var Student = require('../models/student');
const jwt = require('jsonwebtoken');
const config = require('../config');

handle_request =  async(msg, callback) => {
    const { email, password } = msg;
    try {  
        const userStudent = await Student.findOne({
            email
		});
		
        if (!userStudent) {
			callback(null, {status: 400, responseMessage: { msg: 'User not found.' }});
            // return res.status(400).json({ msg: 'User not found.' });
		}
		
        const isMatch = await bcrypt.compare(password.toString(), userStudent.password);
        if (!isMatch) {
			// return res.status(400).json({ msg: 'Invalid Credentials. Please try again.' });
			callback(null, {status: 400, responseMessage: { msg: 'Invalid Credentials. Please try again.' }});
		}
        const payload = {
            user: {
				_id: userStudent._id,
				email: userStudent.email,
				first_name: userStudent.first_name,
				user: userStudent.user,
            },
        };        
        jwt.sign(
            payload,
            config.JWTPASSWORD,
            {
                expiresIn: 360000,
            },
            (err, token) => {
				if (err) throw err;
				callback(null, {status: 200, responseMessage: {token: "JWT " + token, user: payload.user.user}});
                // res.status(200).json({token: "JWT " + token});
            },
        );
    } catch (e) {
		callback(null, {status: 500, responseMessage: 'Unable to log in. Please try again.'});
        // return res.status(500).json('Unable to log in. Please try again.');
    }
}

exports.handle_request = handle_request;