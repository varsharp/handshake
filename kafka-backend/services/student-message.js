var Student = require('../models/student');
var Company = require('../models/company');

handle_request = async (msg, callback) => {
    let { email } = msg;
    let receiver;
    let isCompany = false;
    receiver = await Student.findOne({email});
    
    if(!receiver) {
        isCompany = true;
        receiver = await Company.findOne({email});
    }
    
    const senderStudent = await Student.findById(msg.user._id);

    const senderMessageConvEntry = {
        date: Date.now(),
        body: msg.body,
        action: 'Sent'
    };
    const receiverMessageConvEntry = {
        date: Date.now(),
        body: msg.body,
        action: 'Received'
    };

    try {
        let senderMessageEntry = {
            entity: receiver.first_name + ' ' + receiver.last_name,
            date: Date.now(),
            email,
        };
        let receiverMessageEntry = {
            entity: senderStudent.first_name + ' ' + senderStudent.last_name,
            date: Date.now(),
            email: senderStudent.email,
        };

        // entry in sender
        if (senderStudent.message_list.length) {
            let history = senderStudent.message_list.find(message => message.email == email);
            if(history) {
                history.message_conversation.unshift(senderMessageConvEntry);
                await senderStudent.save();
            } else {
                senderStudent.message_list.push(senderMessageEntry);
                senderStudent.message_list[senderStudent.message_list.length - 1].message_conversation.push(senderMessageConvEntry);
                await senderStudent.save();
            }
        } else {
            senderStudent.message_list.push(senderMessageEntry);
            senderStudent.message_list[0].message_conversation.push(senderMessageConvEntry);
            await senderStudent.save();
        }

        // entry in receiver
        if (receiver.message_list.length) {
            let history = receiver.message_list.find(message => message.email == senderStudent.email);
            if(history) {
                history.message_conversation.unshift(receiverMessageConvEntry);
                await receiver.save();
            } else {
                receiver.message_list.push(receiverMessageEntry);
                receiver.message_list[receiver.message_list.length - 1].message_conversation.push(receiverMessageConvEntry);
                await receiver.save();
            }
        } else {
            receiver.message_list.push(receiverMessageEntry);
            receiver.message_list[0].message_conversation.push(receiverMessageConvEntry);
            await receiver.save();
        }

        if(isCompany) {
            callback(null, { status: 200, responseMessage: senderStudent.message_list[senderStudent.message_list.length - 1].message_conversation });
        } else {
            callback(null, { status: 200, responseMessage: senderStudent.message_list[0].message_conversation });
        }        
    } catch (e) {
        callback(null, { status: 500, responseMessage: 'Unable to save data.' });
    }
};

exports.handle_request = handle_request;