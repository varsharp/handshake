var Student = require('../models/student');

handle_request =  async(msg, callback) => {
    try {
        const student = await Student.findOne({_id: msg.id}, (err, result) => {
            return result;
        })
        if (student) {
            await student.updateOne({
                profile_picture: msg.filename,
            });
            callback(null, {status: 200, responseMessage: 'Success'});
        }
    } catch (e) {
        callback(null, {status: 500, responseMessage: 'Unable to fetch data.'});
    }
}

exports.handle_request = handle_request;